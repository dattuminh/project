# Running for production

`docker-compose -f docker-compose.yml -f docker-compose.backend.yml -f docker-compose.prod.yml up --build`

Then go to `localhost:8080`

This will run a database container, build and run the API server container, and build the Ember application which is copied into an Nginx container where it's served on port 8080.

# Running for front-end development/general app testing and usage
`docker-compose -f docker-compose.yml -f docker-compose.backend.yml -f docker-compose.frontend.yml up`

This will run the database container, an Ember server container, and the API server container. Changes made will be automatically built by the Ember server.

Use `docker-compose -f docker-compose.yml -f docker-compose.backend.yml -f docker-compose.frontend.yml down` to kill the containers when you're done (if they don't stop automatically).

# Running for back-end development
`docker-compose -f docker-compose.yml -f docker-compose.frontend.yml up`

This will run the database container, and an Ember server container (to allow viewing/testing the frontend after backend changes). The API server should be manually built/run so it can be easily changed and tested.

Use `docker-compose -f docker-compose.yml -f docker-compose.frontend.yml down` to kill the containers when you're done (if they don't stop automatically).

# Ports
- `localhost:4200` is the Ember server, go there in your browser to view the app.
- `localhost:4567` is the API server, you can manually make HTTP requests to test the server (if it's running, either through the front-end development command or by manually running the API server on your local machine).
- `localhost:5432` is the PostgreSQL server.

# Features
## Working
- User accounts (login and registration)
- List creation, including adding and removing items from lists
- Basic grocery item database
- Map view with grocery store layout and item marking
- Live checking off of grocery items
- Responsive web app, usable on desktop and mobile

## Not Working / Potential Improvments
- Lists retrieval belongs to the users
    - Creating a list works, but retreiving the list of all created lists does not. To edit a list, create one and you'll be redirected to the list editing page. Dummy values are shown on the homepage to show what it should look like.
- Popup marker based on coordinates of the items of the list
- List deletion

# Implementation Details
## Custom ORM
- We built our own primitive ORM because we found the existing Java solutions (Hibernate), while mature and widely used, are... too JavaEE for the scope of this project. Just like Spring, Hibernate takes a team to configure and set up, plus the learning curve is relatively large considering the length of this project. For this reason, we thought Hibernate was out of scope, and so we chose to try and make something simple ourselves to replace it. The result is a DatabaseManager class that holds methods for accessing and modifying various items in the database, with the actual database statements (hardcoded strings) living in Statements.java, which fetches a string statement and returns PreparedStatements for DatabaseManager to prepare and execute.

## Authorization
- Custom authorization using JSON Web Tokens, with salted hashes of passwords stored in the database for security
- Ember Simple Auth for front-end authorization management, with custom build authorizers to inject a token into API requests and adapter modifications to provide redirects when login is necessary

## EmberJS
- One of the reason why we choose EmberJs as the framework is due to the Ember Data library, which is designed with built-in convention for the endpoints based on the data models implemented by users, simplifying the design phase for both backend and frontend.  
- CRUDs operations are also simplified by the use of querying/retrieving records functions, provided by the Ember Data store, allowing high degree of customization without writing multiple similar and complex ajax calls.
- When records are retrieved, the data persisted within the store and can be reused without making another network calls, providing better performance. 

## Mapbox
- Mapbox allows us to pinpoint location based on OpenStreetMap server, and by creating our own GeoGson file we mapped out the store layout on top of the map layer.
- Marker can be placed based on coordinates we provide on the map layer and the map can display popup where the markers are, with information of the selected items.
