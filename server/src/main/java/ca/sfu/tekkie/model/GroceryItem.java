package ca.sfu.tekkie.model;

public class GroceryItem {

    private Integer id;
    private String name;
    private String description;
    private Float latitude;
    private Float longtitude;
    private String image;

    public GroceryItem(Integer id, String name, String description, Float latitude, Float longtitude, String image) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.latitude = latitude;
        this.longtitude = longtitude;
        this.image = image;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}



