package ca.sfu.tekkie.model;

import java.util.ArrayList;
import java.util.List;

//name, date created, date modified, author, collection of GroceryItems, status of each GroceryItem (status off, or not)
public class GroceryList {

//
//    public class Pair {
//        private GroceryItem groceryItem;
//        private int groceryItemId;
//        private Boolean status;
//
//
//        public Pair(GroceryItem groceryItem, Boolean status) {
////            this.groceryItem = groceryItem;
//            this./groceryItemId = groceryItem.getId();
//            this.status = status;
//        }
//
//        public GroceryItem getGroceryItem() {
//            return this.groceryItem;
//        }
//
//        public Boolean isChecked() {
//            return this.status;
//        }
//    }

    private String id;
    private String name;
    private Long dateCreated;
    private Long dateModified;
    private String author;

    private ArrayList<Integer> itemList;

    public GroceryList(String name, String author) {
        this.name = name;
        this.author = author;
        this.dateCreated = System.currentTimeMillis();
        this.dateModified = System.currentTimeMillis();
        this.itemList = new ArrayList<>();
    }

    public GroceryList(String name, String author, ArrayList<Integer> itemsList){
        this.name = name;
        this.author = author;
        this.itemList = itemsList;
        this.dateCreated = System.currentTimeMillis();
        this.dateModified = System.currentTimeMillis();
    }


    //no args
    public GroceryList() {

    }

    public String getName() {
        return name;
    }

    public String getId() {
        return id;
    }

    public ArrayList<Integer> getItemList() {
        return itemList;
    }


    public String getAuthor() {
        return author;
    }

    public Long getDateCreated() {
        return dateCreated;
    }

    public Long getDateModified() {
        return dateModified;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setDateCreated(Long dateCreated) {
        this.dateCreated = dateCreated;
    }

    public void setDateModified(Long dateModified) {
        this.dateModified = dateModified;
    }

    //TODO: REFACTOR THIS
    public void addItem(GroceryItem item) {
//        if (item == null) {
//            return;
//        }
//
//        itemList.add(new Pair(item, false));
//        this.dateModified = System.currentTimeMillis();
//    }
//
//    public void printItems() {
//        for (Pair item : itemList) {
//            System.out.println(item.getGroceryItem().getName() + ", status: " + item.isChecked());
//        }
    }
}
