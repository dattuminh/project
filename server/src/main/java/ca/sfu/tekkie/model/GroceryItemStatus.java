package ca.sfu.tekkie.model;

public class GroceryItemStatus {
    private int id;
    private int itemId;
    private boolean status;

    public GroceryItemStatus(int itemId, boolean status) {
        this.itemId = itemId;
        this.status = status;
    }

    public GroceryItemStatus(int id, int itemId, boolean status) {
        this.id = id;
        this.itemId = itemId;
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public int getItemId() {
        return itemId;
    }

    public boolean isStatus() {
        return status;
    }
}
