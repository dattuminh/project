package ca.sfu.tekkie;

import java.io.InputStream;
import java.net.URL;

import ca.sfu.tekkie.api.ApiController;
import ca.sfu.tekkie.api.accounts.AccountManager;
import ca.sfu.tekkie.db.DatabaseManager;

import static spark.Spark.get;

public class Main {
    private static final String CONTAINER_ENVIRONMENT_VARIABLE = "CONTAINER";

    public static void main(String[] args) {
        String containerEnv = System.getenv(CONTAINER_ENVIRONMENT_VARIABLE);
        boolean container = containerEnv != null && containerEnv.equals("1");
        InputStream initialDataStream = Thread.currentThread().getContextClassLoader().getResourceAsStream("document.json");
        DatabaseManager databaseManager = new DatabaseManager(container, initialDataStream);
        databaseManager.initialize();
        AccountManager accountManager = new AccountManager(databaseManager);
        ApiController apiController = new ApiController(databaseManager, accountManager);
        apiController.start();
    }
}
