package ca.sfu.tekkie.api;

import com.google.gson.Gson;

import ca.sfu.tekkie.api.Responses.LoginResponse;
import ca.sfu.tekkie.api.Responses.RegisterResponse;
import ca.sfu.tekkie.api.accounts.AccountManager;
import ca.sfu.tekkie.api.routes.ListApi;
import ca.sfu.tekkie.db.DatabaseManager;
import spark.Redirect;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static spark.Spark.*;

public class ApiController {
    private Logger logger = LoggerFactory.getLogger(ApiController.class);

    private DatabaseManager dbManager;
    private AccountManager accountManager;
    private ListApi listApi;
    private Gson gson;

    public ApiController(DatabaseManager dbManager, AccountManager accountManager) {
        this.dbManager = dbManager;
        this.accountManager = accountManager;
        this.listApi = new ListApi(dbManager);
        this.gson = new Gson();
    }

    public void start() {
//        before("/*",
//                (request, response) -> response.header("Access-Control-Allow-Origin", "http://localhost:4200"),
//                (request, response) -> response.header("Vary", "Origin"));

        //ALLOW CORS
        options("/*",
                (request, response) -> {

                    String accessControlRequestHeaders = request
                            .headers("Access-Control-Request-Headers");
                    if (accessControlRequestHeaders != null) {
                        response.header("Access-Control-Allow-Headers",
                                accessControlRequestHeaders);
                    }

                    String accessControlRequestMethod = request
                            .headers("Access-Control-Request-Method");
                    if (accessControlRequestMethod != null) {
                        response.header("Access-Control-Allow-Methods",
                                accessControlRequestMethod);
                    }

                    return "OK";
                });

        before((request, response) -> response.header("Access-Control-Allow-Origin", "*"));


        post("/register", (request, response) -> {
            String username = request.queryParams("username");
            String password = request.queryParams("password");
            if (username.isEmpty() || password.isEmpty()) {
                response.status(400);
                return gson.toJson(RegisterResponse.getFailureRegisterResponse("You must provide a username and password."));
            }
            String error = accountManager.registerUser(username, password);
            if (error.isEmpty()) {
                return gson.toJson(RegisterResponse.getSuccessRegisterResponse());
            }
            response.status(400);
            return gson.toJson(RegisterResponse.getFailureRegisterResponse("Failed to register account: " + error));
        });
        post("/login", (request, response) -> {
            String username = request.queryParams("username");
            String password = request.queryParams("password");
            String jws = accountManager.login(username, password);
            if (jws.isEmpty()) {
                response.status(403);
                return gson.toJson(LoginResponse.getFailureLoginResponse("The username or password provided is incorrect."));
            }
            return gson.toJson(LoginResponse.getSuccessLoginResponse(jws));
        });
        path("/api", () -> {
            before("/*", (request, response) -> {
                if (request.requestMethod().equals("OPTIONS")) {
                    return;
                }
                String authorizationHeader = request.headers("Authorization");
                if (authorizationHeader != null && !authorizationHeader.isEmpty()) {
                    if (accountManager.authenticated(authorizationHeader)) {
                        return;
                    }
                }
                response.redirect("/login", 401);
            });

            get("/groceryItems/:id", listApi.getGroceryItem);
            get("/groceryItems", listApi.getGroceryItem);

            get("/groceryLists/:id", listApi.getList);
            post("/groceryLists", listApi.createList);
            put("/groceryLists/:id", listApi.updateList);

            get("/groceryItemStatuses/:id", listApi.getGroceryItemStatus);
            get("/groceryItemStatuses", listApi.getGroceryItemStatus);
            post("/groceryItemStatuses", listApi.createGroceryItemStatus);
            put("/groceryItemStatuses/:id", listApi.updateGroceryItemStatus);
            delete("/groceryItemStatuses/:id", listApi.deleteGroceryItemStatus);

            path("/glist", () -> {
                get("/list", listApi.getAllGroceryList);
                post("/addItem/:id/:name", listApi.addItemIntoList);
                patch("/remove/:id/:name", listApi.deleteItemList);
                delete("/delete/:id", listApi.deleteGroceryList);
                get("/getList/:id", listApi.getList);
            });


            after("/*",
                    (request, response) -> response.header("Content-Encoding", "gzip"),
                    (request, response) -> response.type("application/json"));
        });
    }
}
