package ca.sfu.tekkie.api.accounts;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.URL;
import java.security.spec.InvalidKeySpecException;

import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;

import ca.sfu.tekkie.db.DatabaseManager;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.impl.crypto.MacProvider;

public class AccountManager {
    private final DatabaseManager dbManager;
    SecretKey signingKey = null;
    Logger logger = LoggerFactory.getLogger(AccountManager.class);

    public AccountManager(DatabaseManager dbManager) {
        this.dbManager = dbManager;
        this.signingKey = MacProvider.generateKey();
    }

    public String registerUser(String username, String password) {
        try {
            if (dbManager.checkUserExists(username)) {
                return "Username taken.";
            }
            String hashedPassword = PasswordHelper.getSaltedHash(password);
            String[] saltAndHash = hashedPassword.split("\\$");
            dbManager.addNewUser(username, saltAndHash[0], saltAndHash[1]);
            return "";
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "Server error.";
    }

    public String login(String username, String password) {
        String saltedHash = dbManager.getSaltedPassword(username);
        if (saltedHash == null || saltedHash.isEmpty()) {
            return "";
        }
        try {
            if (PasswordHelper.check(password, saltedHash)) {
                return Jwts.builder().setSubject(username).signWith(SignatureAlgorithm.HS512, signingKey).compact();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    /**
     * Check if the user is authenticated. Since they can only get a JWS from the server when they log in,
     * all we need to do is check if the decrypted JWS contains a valid username to confirm that
     * they are authenticated.
     * @param jws A JSON Web Signature
     * @return True if authenticated, false if not
     */
    public boolean authenticated(String jws) {
        try {
            Claims claims = Jwts.parser().setSigningKey(signingKey).parseClaimsJws(jws).getBody();
            String username = claims.getSubject();
            return dbManager.checkUserExists(username);
        }
        catch (SignatureException e) {
            return false;
        }
    }
}
