package ca.sfu.tekkie.api.Responses;

public class RegisterResponse {
    private String error;

    public RegisterResponse(String error) {
        this.error = error;
    }

    public static RegisterResponse getSuccessRegisterResponse() {
        return new RegisterResponse("");
    }

    public static RegisterResponse getFailureRegisterResponse(String error) {
        return new RegisterResponse(error);
    }
}
