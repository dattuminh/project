package ca.sfu.tekkie.api.Responses;

public class LoginResponse {
    private String token;
    private String error;

    public LoginResponse(String token, String error) {
        this.token = token;
        this.error = error;
    }

    public static LoginResponse getSuccessLoginResponse(String jws) {
        return new LoginResponse(jws, "");
    }

    public static LoginResponse getFailureLoginResponse(String error) {
        return new LoginResponse("", error);
    }

}
