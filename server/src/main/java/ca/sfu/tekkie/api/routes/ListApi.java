package ca.sfu.tekkie.api.routes;

import ca.sfu.tekkie.db.DatabaseManager;
import ca.sfu.tekkie.model.GroceryList;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import spark.Request;
import spark.Response;
import spark.Route;

import java.sql.Timestamp;
import java.util.ArrayList;

public class ListApi {
    private Gson gson;
    private DatabaseManager dbManager;

    private static String GROCERY_LIST_SINGLE = "grocery-list";
    private static String GROCERY_LIST_MULTIPLE = "grocery-lists";
    private static String GROCERY_ITEM_SINGLE = "grocery-item";
    private static String GROCERY_ITEM_MULTIPLE = "grocery-items";
    private static String GROCERY_ITEM_STATUS_SINGLE = "grocery-item-status";
    private static String GROCERY_ITEM_STATUS_MULTIPLE = "grocery-item-statuses";

    //TODO: change to adapt to frontend?
    public Route getGroceryItem = (Request req, Response res) -> {
        if(req.params("id") != null){
            int id = Integer.parseInt(req.params("id"));
            String json = gson.toJson(dbManager.getItemById(id));
            return formatPayload(json, GROCERY_ITEM_SINGLE);
        }
        if(req.queryParams("listId") != null){
            int listId = Integer.parseInt(req.queryParams("listId"));
            String json = gson.toJson(dbManager.getAllGroceryItemOfList(listId));
            return formatPayload(json, GROCERY_ITEM_SINGLE); //Since ArrayList already creates the square bracket
        }

        if(req.queryParams("name") == null){
            String json = gson.toJson(dbManager.getAllItem());
            return formatPayload(json, GROCERY_ITEM_SINGLE);
        }
        else{
            String json = gson.toJson(dbManager.getItemByName(req.queryParams("name")));
            return formatPayload(json, GROCERY_ITEM_MULTIPLE);
        }
    };

    public Route getList = (Request req, Response res) -> {
        String json = gson.toJson(dbManager.getListById(req.params(":id")));
        return formatPayload(json, GROCERY_LIST_SINGLE);
    };

    public Route getAllGroceryList = (Request req, Response res) -> {
        return gson.toJson(dbManager.getAllGroceryList());
    };

    public Route createList = (Request req, Response res) -> {
        String bodyString = req.body();
        JsonObject bodyWithHeader = new Gson().fromJson(bodyString, JsonObject.class);
        JsonObject body = bodyWithHeader.getAsJsonObject("groceryList");
        String name = body.get("name").getAsString();
        String author = body.get("author").getAsString();
        ArrayList itemsList = new ArrayList<Integer>();
        GroceryList newGroceryList = new GroceryList(name, author, itemsList);
        GroceryList insertedList = dbManager.createGroceryList(newGroceryList);
        String json = gson.toJson(insertedList);
        return formatPayload(json, GROCERY_LIST_SINGLE);
    };

    public Route updateList = (Request req, Response res) -> {
        String bodyString = req.body();
        JsonObject bodyWithHeader = new Gson().fromJson(bodyString, JsonObject.class);
        JsonObject body = bodyWithHeader.getAsJsonObject("groceryList");
        String name = body.get("name").getAsString();
        String author = body.get("author").getAsString();

        ArrayList itemsList = new ArrayList<Integer>();
        JsonArray itemsListJson = body.get("itemList").getAsJsonArray();
        for(int i = 0; i < itemsListJson.size(); i++){
            itemsList.add(itemsListJson.get(i).getAsInt());
        }
        String json = gson.toJson(dbManager.updateGroceryList(name, author, itemsList));
        return formatPayload(json, GROCERY_LIST_SINGLE);
    };

    public Route addItemIntoList = (Request req, Response res) -> {
        return gson.toJson(dbManager.addItemIntoList(req.params(":id"), req.params(":name")));
    };

    public Route deleteItemList = (Request req, Response res) -> {
        return gson.toJson(dbManager.deleteItemFromList(req.params(":id"), req.params(":name")));
    };

    public Route deleteGroceryList = (Request req, Response res) -> {
        return gson.toJson(dbManager.deleteList(req.params(":id")));
    };

    public Route getGroceryItemStatus = (Request req, Response res) -> {
        if(req.queryParams("listId") != null){
            String json = gson.toJson(dbManager.getAllGroceryItemStatusOfList(Integer.valueOf(req.queryParams("listId"))));
            return formatPayload(json, GROCERY_ITEM_STATUS_MULTIPLE);
        }
        if(req.params("id") != null){
            String json = gson.toJson(dbManager.getGroceryItemStatusById(Integer.parseInt(req.params("id"))));
            return formatPayload(json, GROCERY_ITEM_STATUS_SINGLE);
        }
        return null;
    };

    public Route createGroceryItemStatus = (Request req, Response res) -> {
        String bodyString = req.body();
        JsonObject bodyWithHeader = new Gson().fromJson(bodyString, JsonObject.class);
        JsonObject body = bodyWithHeader.getAsJsonObject("groceryItemStatus");
        int itemId = body.get("itemId").getAsInt();
        boolean status = body.get("status").getAsBoolean();
        String json = gson.toJson(this.dbManager.createGroceryItemStatus(itemId, status));
        return formatPayload(json, GROCERY_ITEM_STATUS_SINGLE);
    };

    public Route updateGroceryItemStatus = (Request req, Response res) -> {
        int id = Integer.parseInt(req.params("id"));

        String bodyString = req.body();
        JsonObject bodyWithHeader = new Gson().fromJson(bodyString, JsonObject.class);
        JsonObject body = bodyWithHeader.getAsJsonObject("groceryItemStatus");
        int itemId = body.get("itemId").getAsInt();
        boolean status = body.get("status").getAsBoolean();

        String json = gson.toJson(this.dbManager.updateGroceryItemStatus(id, itemId, status));
        return formatPayload(json, GROCERY_ITEM_STATUS_SINGLE);
    };

    public Route deleteGroceryItemStatus = (Request req, Response res) -> {
        int id = Integer.parseInt(req.params("id"));
        boolean deleted = dbManager.deleteGroceryItemStatus(id);
        return gson.toJson("");
    };


    public ListApi(DatabaseManager dbManager) {
        this.dbManager = dbManager;
        this.gson = new Gson();
    }

    private String formatPayload(String json, String wrapperValue){
        String prependString = "";
        String appendString = "";
        if(wrapperValue == GROCERY_ITEM_MULTIPLE || wrapperValue == GROCERY_LIST_MULTIPLE){
            prependString = "{\"" + wrapperValue + "\":[";
            appendString = "]}";
        }
        else{
            prependString = "{\"" + wrapperValue + "\":";
            appendString = "}";
        }
        return prependString + json + appendString;
    }

}
