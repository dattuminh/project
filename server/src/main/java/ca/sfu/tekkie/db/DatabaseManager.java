package ca.sfu.tekkie.db;

import ca.sfu.tekkie.model.GroceryItem;
import ca.sfu.tekkie.model.GroceryItemStatus;
import ca.sfu.tekkie.model.GroceryList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;


public class DatabaseManager {
    private static final int NUM_DB_CONNECT_RETRIES = 5;
    public static final int RETRY_DELAY = 1000;

    private List<GroceryItem> groceryItems = new ArrayList<>();
    private List<GroceryList> groceryLists = new ArrayList<>();
    public static final String DB_USER = "postgres";
    public static final String DB_PASSWORD = "mysecretpassword";
    private Logger logger = LoggerFactory.getLogger(DatabaseManager.class);
    private Connection dbConnection;
    private Statements statements;
    private static final String REGEX = ",\\s*";
    private static final String ARRAY_LENGTH = "array_length";
    private static final String DELIMITER = ", ";
    private static final int ERROR_VALUE = -1;
    private boolean isContainer;
    private InputStream initialDataStream;

    public DatabaseManager(boolean container, InputStream initialDataStream) {
        this.isContainer = container;
        this.initialDataStream = initialDataStream;
    }

    public void initialize() {
            String url = isContainer ? "jdbc:postgresql://db:5432/postgres" : "jdbc:postgresql://localhost:5432/postgres";
            for (int i = 0; i < NUM_DB_CONNECT_RETRIES; i++) {
                try {
                    Thread.sleep(RETRY_DELAY);
                    this.dbConnection = DriverManager.getConnection(url, DB_USER, DB_PASSWORD);
                    if (this.dbConnection != null) {
                        break;
                    }
                    logger.error("Failed to connect to DB.");
                } catch (SQLException e) {
                    logger.error("Failed to connect to DB.");
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            logger.info("Successfully connected to DB.");
            this.statements = new Statements(dbConnection);
            initializeTable(Statements.GROCERY_ITEMS_TABLE);
            initializeTable(Statements.GROCERY_LISTS_TABLE);
            initializeTable(Statements.GROCERY_ITEM_STATUS_TABLE);
            initializeTable(Statements.ACCOUNTS_TABLE);

        try {
            if (groceryItemsTableEmpty()){
               readJSON(initialDataStream);
            }
        } catch (SQLException e1) {
            e1.printStackTrace();
        }
    }

    private void initializeTable(String tableName) {
        try {
            PreparedStatement existsStatement = this.statements.getCheckTableExistsStatement();
            existsStatement.setString(1, tableName);
            existsStatement.execute();
            ResultSet result = existsStatement.getResultSet();
            result.next();
            boolean isMissing = !result.getBoolean("exists");
            if (isMissing) {
                PreparedStatement createStatement = this.statements.getCreateTableStatement(tableName);
                createStatement.execute();
                PreparedStatement indexStatement = this.statements.getCreateIndexStatement(tableName);
                if(tableName != this.statements.GROCERY_ITEM_STATUS_TABLE){
                    indexStatement.execute();
                }
            }
        } catch (SQLException e) {
            logger.error("Error initializing DB table " + tableName);
            e.printStackTrace();
        }
    }

    public void readJSON(InputStream stream){
        JSONParser parser = new JSONParser();
        try{
            JSONArray jas = (JSONArray) parser.parse(new InputStreamReader(stream));

            for (Object ja : jas){
                JSONObject info = (JSONObject) ja;

                String name = (String) info.get("name");
                String description = (String) info.get("description");
                Float latitude = Float.parseFloat((String) info.get("latitude"));
                Float longitude = Float.parseFloat((String) info.get("longitude"));
                String image = null;

                PreparedStatement insertStatement = statements.getInsertStatement(Statements.GROCERY_ITEMS_TABLE);
                insertStatement.setString(1, name);
                insertStatement.setString(2, description);
                insertStatement.setFloat(3, latitude);
                insertStatement.setFloat(4, longitude);
                insertStatement.setString(5, "");
                insertStatement.execute();
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }


    private boolean groceryItemsTableEmpty() throws SQLException{
        final String getItemsQuery = "Select * from " + Statements.GROCERY_ITEMS_TABLE + ";";
        PreparedStatement statement = dbConnection.prepareStatement(getItemsQuery);
        ResultSet resultSet = statement.executeQuery();

        return !resultSet.next();
    }

    public GroceryItem getItemById(int itemId) throws SQLException{
        PreparedStatement getItemById = statements.getSelectByIdStatement(Statements.GROCERY_ITEMS_TABLE);
        getItemById.setInt(1, itemId);
        ResultSet receivedData = getItemById.executeQuery();
        if(!receivedData.isFirst()){
            receivedData.next();
        }
        GroceryItem gettingItem = new GroceryItem(
                receivedData.getInt("id"),
                receivedData.getString("name"),
                receivedData.getString("description"),
                receivedData.getFloat("latitude"),
                receivedData.getFloat("longitude"),
                receivedData.getString("image")
        );
        return gettingItem;
    }

    public GroceryItem getItemByName(String itemName) throws SQLException {
        PreparedStatement getItemName = statements.getSelectByNameStatement(Statements.GROCERY_ITEMS_TABLE);
        getItemName.setString(1, itemName);
        ResultSet receivedData = getItemName.executeQuery();
        if(!receivedData.isFirst()){
            receivedData.next();
        }
        GroceryItem gettingItem = new GroceryItem(
                receivedData.getInt("id"),
                receivedData.getString("name"),
                receivedData.getString("description"),
                receivedData.getFloat("latitude"),
                receivedData.getFloat("longitude"),
                receivedData.getString("image")
        );
        return gettingItem;
    }

    public ArrayList<GroceryItem> getAllItem() throws SQLException{
        ArrayList<GroceryItem> groceryItems = new ArrayList<>();
        PreparedStatement getAllItem = statements.getAllResult(Statements.GROCERY_ITEMS_TABLE);
        ResultSet returnData = getAllItem.executeQuery();
        if(returnData.isFirst()){
            GroceryItem item = new GroceryItem(
                    (Integer) returnData.getInt("id"),
                    (String) returnData.getString("name"),
                    (String) returnData.getString("description"),
                    (Float) returnData.getFloat("latitude"),
                    (Float) returnData.getFloat("longitude"),
                    (String) returnData.getString("image")
            );
            groceryItems.add(item);

        }
        while(returnData.next()){
            GroceryItem item = new GroceryItem(
                    (Integer) returnData.getInt("id"),
                    (String) returnData.getString("name"),
                    (String) returnData.getString("description"),
                    (Float) returnData.getFloat("latitude"),
                    (Float) returnData.getFloat("longitude"),
                    (String) returnData.getString("image")
            );
            groceryItems.add(item);
        }
        return groceryItems;
    }

    public GroceryList getListById(String id) throws SQLException {
        PreparedStatement returnTheList = statements.getSelectByIdStatement(Statements.GROCERY_LISTS_TABLE);
        returnTheList.setInt(1, Integer.parseInt(id));
        ResultSet recieveData = returnTheList.executeQuery();
        if(!recieveData.next()){
            return null;
        }
        return convertResultSetToGroceryList(recieveData);
    }

    private GroceryList convertResultSetToGroceryList(ResultSet resultSet) throws SQLException{
        if(!resultSet.isFirst()){
            resultSet.next();
        }
        String id = String.valueOf(resultSet.getInt("id"));
        String name = resultSet.getString("name");
        String author = resultSet.getString("author");
        Long dateCreated = resultSet.getTimestamp("datecreated").getTime();
        Long dateModified = resultSet.getTimestamp("datemodified").getTime();

        GroceryList groceryList = new GroceryList(name, author);
        groceryList.setId(id);
        groceryList.setDateCreated(dateCreated);
        groceryList.setDateModified(dateModified);

        int arrayLength = getGroceryListLength(Integer.parseInt(id));

        for(int itemIndex = 1; itemIndex <= arrayLength; itemIndex++){
            int itemStatusId = getGroceryItemStatusIdFromList(Integer.parseInt(id), itemIndex);
            if(itemStatusId != -1){
                groceryList.getItemList().add(itemStatusId);
            }
        }
        return groceryList;
    }

    private int getGroceryItemStatusIdFromList(int listId, int groceryItemStatusIndex){
        PreparedStatement statement = null;
        try {
            statement = statements.getSelectItemFromGroceryListStatement();
            statement.setInt(1, groceryItemStatusIndex);
            statement.setInt(2, listId);
            ResultSet resultSet = statement.executeQuery();
            if(!resultSet.next()){
                return -1;
            }
            else{
                return resultSet.getInt(Statements.GROCERY_LIST_ITEMS_COLUMN);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return -1;
    }

    //TODO: THINK ABOUT HOW TO ADAPT/CHANGE THIS
    public List<GroceryList> getAllGroceryList(){
        List<GroceryList> groceryLists = new ArrayList<>();
        try {
            PreparedStatement getAllList = statements.getSelectByNameStatement(Statements.GROCERY_LISTS_TABLE);
            getAllList.setString(1, "david");
            ResultSet receivedData = getAllList.executeQuery();
            while (receivedData.next()){
                groceryLists.add(convertResultSetToGroceryList(receivedData));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return groceryLists;
    }
//    public GroceryList.Pair getGroceryItemFromList(int listId, int itemIndex){
//        try {
//            PreparedStatement statement = statements.getSelectItemFromGroceryListStatement();
//            statement.setInt(1, itemIndex);
//            statement.setInt(2, listId);
//            ResultSet resultSet = statement.executeQuery();
//            if(!resultSet.next()){
//                return null;
//            }
//            else{
//                JsonObject jsonPair = new Gson().fromJson(resultSet.getString(Statements.GROCERY_LIST_ITEMS_COLUMN), JsonObject.class);
//                int itemId = jsonPair.get("groceryItemId").getAsInt();
//                Boolean status = jsonPair.get("status").getAsBoolean();
//                GroceryItem groceryItem = getItemById(itemId);
//                return new GroceryList().new Pair(groceryItem, status);
//            }
//
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//        return null;
//    }

    public GroceryList createGroceryList(GroceryList groceryList){
        try{

            //TODO: CHANGE THIS TO ADAPT TO FULL OBJECT
            String itemList = "";
            PreparedStatement insertStatement;

            if(itemList.isEmpty()){
                insertStatement = statements.getInsertStatementGroceryListNoItemStatement();
            }
            else{
                insertStatement = statements.getInsertStatement(Statements.GROCERY_LISTS_TABLE);
                insertStatement.setString(5, itemList);
            }

            insertStatement.setString(1, groceryList.getName());
            insertStatement.setString(2, groceryList.getAuthor());
            insertStatement.setTimestamp(3, new Timestamp(groceryList.getDateCreated()));
            insertStatement.setTimestamp(4, new Timestamp(groceryList.getDateModified()));

            insertStatement.execute();


            PreparedStatement selectStatement = statements.getSelectByNameAndAuthorStatement();
            selectStatement.setString(1, groceryList.getName());
            selectStatement.setString(2, groceryList.getAuthor());
            ResultSet resultSet = selectStatement.executeQuery();
            if(!resultSet.isFirst()){
                resultSet.next();
            }
            return convertResultSetToGroceryList(resultSet);

        } catch (SQLException e){
            e.printStackTrace();
        }
        return null;
    }

    public GroceryList updateGroceryList(String name, String author, ArrayList<Integer> itemList){
        try {
            PreparedStatement getListStatement = this.statements.getSelectByNameAndAuthorStatement();
            getListStatement.setString(1, name);
            getListStatement.setString(2, author);
            ResultSet resultSet = getListStatement.executeQuery();
            GroceryList returnedGroceryList = convertResultSetToGroceryList(resultSet);

            PreparedStatement updateListStatement = this.statements.getUpdateStatement(Statements.GROCERY_LISTS_TABLE);
            updateListStatement.setString(1, name);
            updateListStatement.setString(2, author);
            updateListStatement.setTimestamp(3, new Timestamp(returnedGroceryList.getDateCreated()));
            updateListStatement.setTimestamp(4, new Timestamp(returnedGroceryList.getDateModified()));
            Array itemArray = this.dbConnection.createArrayOf("int", itemList.toArray());
            updateListStatement.setArray(5, itemArray);
            updateListStatement.setInt(6, Integer.parseInt(returnedGroceryList.getId()));
            updateListStatement.execute();

            resultSet = getListStatement.executeQuery();
            return convertResultSetToGroceryList(resultSet);

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public GroceryList addItemIntoList(String tempId, String Itemname) throws SQLException {

        int id = Integer.parseInt(tempId);

        PreparedStatement getOldGroceryList = statements.getSelectByIdStatement(Statements.GROCERY_LISTS_TABLE);
        getOldGroceryList.setInt(1, id);
        ResultSet dataOldGrocerList = getOldGroceryList.executeQuery();
        dataOldGrocerList.next();
        String tempList = (String) dataOldGrocerList.getString("listitem");

        PreparedStatement getListUpdate = statements.getUpdateStatement(Statements.GROCERY_LISTS_TABLE);
        getListUpdate.setString(1, tempList + Itemname + ", ");
        getListUpdate.setInt(2, id);
        getListUpdate.execute();

        return getListById(String.valueOf(id));
    }

    public GroceryList deleteItemFromList(String tempId, String Itemname) throws SQLException {

        int id = Integer.parseInt(tempId);

        PreparedStatement getOldGroceryList = statements.getSelectByIdStatement(Statements.GROCERY_LISTS_TABLE);
        getOldGroceryList.setInt(1, id);
        ResultSet dataOldGrocerList = getOldGroceryList.executeQuery();
        dataOldGrocerList.next();
        String tempList = (String) dataOldGrocerList.getString("listitem");

        String[] tempItemList = tempList.split(REGEX);
        List<String> temping = new ArrayList<>();
        for (int i = 0; i < tempItemList.length; i++){
            if (tempItemList[i].equals(Itemname)){
                tempItemList[i] = null;
            }
            else {
                temping.add(tempItemList[i]);
            }
        }

        String result = "";
        for (int i = 0; i < temping.size(); i++){
            result = result + temping.get(i) + ", ";
        }

        PreparedStatement getListUpdate = statements.getUpdateStatement(Statements.GROCERY_LISTS_TABLE);
        getListUpdate.setInt(2, id);
        getListUpdate.setString(1, result);
        getListUpdate.execute();

        return getListById(String.valueOf(id));
    }

    public List<GroceryList> deleteList(String id) throws SQLException {
        PreparedStatement deleteList = statements.getDeleteStatement(Statements.GROCERY_LISTS_TABLE);
        deleteList.setInt(1, Integer.parseInt(id));
        deleteList.execute();
        return getAllGroceryList();
    }

    public GroceryItemStatus getGroceryItemStatusById(int groceryItemStatusId){
        try {
            PreparedStatement statements = this.statements.getSelectByIdStatement(Statements.GROCERY_ITEM_STATUS_TABLE);
            statements.setInt(1, groceryItemStatusId);
            ResultSet rs = statements.executeQuery();
            if(!rs.next()){
                return null;
            }
            else{
                int id = rs.getInt("id");
                int itemId = rs.getInt("itemId");
                boolean status = rs.getBoolean("status");

                return new GroceryItemStatus(id, itemId, status);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public ArrayList<GroceryItemStatus> getAllGroceryItemStatusOfList(int listId){
        try {
            ArrayList groceryItemStatusList = new ArrayList();
            GroceryList groceryList = getListById(Integer.toString(listId));
            ArrayList<Integer> itemStatusList = groceryList.getItemList();
            for(int id : itemStatusList){
                groceryItemStatusList.add(getGroceryItemStatusById(id));
            }
            return groceryItemStatusList;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public ArrayList<GroceryItem> getAllGroceryItemOfList(int listId){
        ArrayList<GroceryItemStatus> groceryItemStatusList = getAllGroceryItemStatusOfList(listId);
        ArrayList groceryItemList = new ArrayList();

        for(GroceryItemStatus itemStatus : groceryItemStatusList){
            try {
                groceryItemList.add(getItemById(itemStatus.getItemId()));
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return groceryItemList;
    }

    public GroceryItemStatus createGroceryItemStatus(int itemId, boolean status){
        try {
            PreparedStatement statement = this.statements.getInsertStatement(Statements.GROCERY_ITEM_STATUS_TABLE);
            statement.setInt(1, itemId);
            statement.setBoolean(2, status);
            ResultSet resultSet = statement.executeQuery();
            if(!resultSet.next()){
                return null;
            }
            else{
                int id = resultSet.getInt("id");
                int returnedItemId = resultSet.getInt("itemId");
                boolean returnedStatus = resultSet.getBoolean("status");
                return new GroceryItemStatus(id, returnedItemId, returnedStatus);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public GroceryItemStatus updateGroceryItemStatus(int id, int itemId, boolean status){
        try {
            PreparedStatement statement = this.statements.getUpdateStatement(Statements.GROCERY_ITEM_STATUS_TABLE);
            statement.setInt(1, itemId);
            statement.setBoolean(2, status);
            statement.setInt(3, id);
            ResultSet resultSet = statement.executeQuery();
            if(!resultSet.next()){
                return null;
            }
            else{
                int returnedItemId = resultSet.getInt("itemId");
                boolean returnedStatus = resultSet.getBoolean("status");
                return new GroceryItemStatus(id, returnedItemId, returnedStatus);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public boolean deleteGroceryItemStatus(int id){
        try {
            PreparedStatement deleteStatement = statements.getDeleteStatement(Statements.GROCERY_ITEM_STATUS_TABLE);
            deleteStatement.setInt(1, id);
            deleteStatement.execute();
            PreparedStatement getStatement = statements.getSelectByIdStatement(Statements.GROCERY_ITEM_STATUS_TABLE);
            getStatement.setInt(1, id);
            return !getStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public int getGroceryListLength(int id) throws SQLException {
        String query = "Select array_length(listitem, 1) from " + this.statements.GROCERY_LISTS_TABLE + " where id = " + String.valueOf(id);
        PreparedStatement statement = this.dbConnection.prepareStatement(query);
        ResultSet resultSet = statement.executeQuery();
        if(!resultSet.next()){
            return -1;
        }
        return resultSet.getInt(ARRAY_LENGTH);
    }

    public void addNewUser(String username, String salt, String hash) {
        try {
            PreparedStatement statement = statements.getInsertStatement(Statements.ACCOUNTS_TABLE);
            statement.setString(1, username);
            statement.setString(2, salt);
            statement.setString(3, hash);
            statement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public String getSaltedPassword(String username) {
        try {
            PreparedStatement statement = statements.getSelectByNameStatement(Statements.ACCOUNTS_TABLE);
            statement.setString(1, username);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                return resultSet.getString("salt") + "$" + resultSet.getString("hash");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return "";
    }

    public boolean checkUserExists(String username) {
        try {
            PreparedStatement statement = statements.getSelectByNameStatement(Statements.ACCOUNTS_TABLE);
            statement.setString(1, username);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

}
