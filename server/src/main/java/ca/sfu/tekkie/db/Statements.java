package ca.sfu.tekkie.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

class Statements {
    static final String SCHEMA_NAME = "public";
    static final public String GROCERY_LIST_ITEMS_COLUMN = "listitem";

    static final String CHECK_TABLE_EXISTS_STATEMENT = "SELECT EXISTS (" +
            "SELECT 1 " +
            "FROM   information_schema.tables " +
            "WHERE  table_schema = \'" + SCHEMA_NAME + "\'" +
            "AND    table_name = ? " +
            ");";

    static final String GROCERY_ITEMS_TABLE = "groceryitems";
    static final String GROCERY_ITEMS_CREATE_TABLE_STATEMENT = "CREATE TABLE " + GROCERY_ITEMS_TABLE + "(" +
            "id SERIAL PRIMARY KEY, " +
            "name VARCHAR(255), " +
            "description VARCHAR(255), " +
            "latitude FLOAT, " +
            "longitude FLOAT, " +
            "image VARCHAR(255) " +
            ");";
    static final String GROCERY_LISTS_TABLE = "grocerylists";
    static final String GROCERY_LISTS_CREATE_TABLE_STATEMENT = "CREATE TABLE " + GROCERY_LISTS_TABLE + "(" +
            "id SERIAL PRIMARY KEY, " +
            "name VARCHAR(255), " +
            "author VARCHAR(255), " +
            "datecreated TIMESTAMP(3) WITH TIME ZONE, " +
            "datemodified TIMESTAMP(3) WITH TIME ZONE, " +
            "listitem INTEGER[]" +
            ");";

    static final String GROCERY_ITEM_STATUS_TABLE = "groceryitemstatuses";
    static final String GROCERY_ITEM_STATUS_CREATE_TABLE_STATEMENT = "CREATE TABLE " + GROCERY_ITEM_STATUS_TABLE + "(" +
            "id SERIAL PRIMARY KEY, " +
            "itemId INTEGER, " +
            "status BOOLEAN" +
            ");";

    static final String ACCOUNTS_TABLE = "accounts";
    static final String ACCOUNTS_TABLE_CREATE_STATEMENT = "CREATE TABLE " + ACCOUNTS_TABLE + "(" +
            "id SERIAL PRIMARY KEY, " +
            "username VARCHAR(255), " +
            "salt VARCHAR, " +
            "hash VARCHAR" +
            ");";
    private Map<String, String> createTableStatementMap;

    static final String GROCERY_ITEMS_CREATE_INDEX_STATEMENT = "CREATE INDEX " +
            "grocery_item_name_index ON " + GROCERY_ITEMS_TABLE + "(name);";
    static final String GROCERY_LISTS_INDEX_STATEMENT = "CREATE INDEX " +
            "grocery_lists_author_index ON " + GROCERY_LISTS_TABLE + "(author);";
    static final String ACCOUNTS_CREATE_INDEX_STATEMENT = "CREATE INDEX " +
            "accounts_username_index ON " + ACCOUNTS_TABLE + "(username);";
    private Map<String, String> createIndexStatementMap;

    static final String GROCERY_ITEMS_INSERT_STATEMENT = "INSERT INTO " + GROCERY_ITEMS_TABLE +
            "(name, description, latitude, longitude, image) values (?, ?, ?, ?, ?);";
    static final String GROCERY_LISTS_INSERT_STATEMENT = "INSERT INTO " + GROCERY_LISTS_TABLE +
            " (name, author, datecreated, datemodified, listitem) VALUES (?, ?, ?, ?, array[?]);";
    static final String GROCERY_ITEM_STATUS_INSERT_STATEMENT = "INSERT INTO " + GROCERY_ITEM_STATUS_TABLE+
            "(itemId, status) values (?, ?) RETURNING *";
    static final String GROCERY_LISTS_INSERT_NOITEMS_STATEMENT = "INSERT INTO " + GROCERY_LISTS_TABLE +
            " (name, author, datecreated, datemodified, listitem) VALUES (?, ?, ?, ?, array[]::INTEGER[]);";
    static final String ACCOUNTS_INSERT_NEW_USER = "INSERT INTO " + ACCOUNTS_TABLE +
            " (username, salt, hash) VALUES (?, ?, ?);";
    private Map<String, String> insertStatementMap;

    static final String GROCERY_ITEMS_SELECT_BY_ID_STATEMENT = "SELECT * FROM " + GROCERY_ITEMS_TABLE + " WHERE id = ?;";
    static final String GROCERY_LISTS_SELECT_BY_ID_STATEMENT = "SELECT * FROM " + GROCERY_LISTS_TABLE + " WHERE id = ?;";
    static final String GROCERY_ITEM_STATUS_SELECT_BY_ID_STATEMENT = "SELECT * FROM " + GROCERY_ITEM_STATUS_TABLE + " WHERE id = ?;";
    static final String GROCERY_LISTS_SELECT_ITEM_STATUS_ID_FROM_STATEMENT = "SELECT listitem[?] FROM " + GROCERY_LISTS_TABLE + " WHERE id = ?;";
    static final String ACCOUNTS_SELECT_BY_ID = "SELECT * FROM " + ACCOUNTS_TABLE + "WHERE id = ?;";
    private Map<String, String> selectByIdStatementMap;

    static final String GROCERY_ITEMS_SELECT_BY_NAME_STATEMENT = "SELECT * FROM " + GROCERY_ITEMS_TABLE + " WHERE name = ?;";
    static final String GROCERY_LISTS_SELECT_BY_AUTHOR_STATEMENT = "SELECT * FROM " + GROCERY_LISTS_TABLE + " WHERE author = ?;";
    static final String GROCERY_LISTS_SELECT_BY_NAME_AND_AUTHOR_STATEMENT = "SELECT * FROM " + GROCERY_LISTS_TABLE + " WHERE name = ? and author = ?;";
    static final String GROCERY_ITEM_STATUS_SELECT_BY_ITEMID_AND_STATUS_STATEMENT = "SELECT * FROM " + GROCERY_ITEM_STATUS_TABLE + " WHERE itemId = ? and status = ?";
    static final String ACCOUNTS_SELECT_BY_USERNAME = "SELECT * FROM " + ACCOUNTS_TABLE + " WHERE username = ?;";
    private Map<String, String> selectByNameStatementMap;

    static final String GROCERY_LISTS_UPDATE_STATEMENT = "UPDATE " + GROCERY_LISTS_TABLE + " SET (name, author, datecreated, datemodified, listitem) = (?, ?, ?, ?, ?) WHERE id = ?;";
    static final String GROCERY_ITEM_STATUS_UPDATE_STATEMENT = "UPDATE " + GROCERY_ITEM_STATUS_TABLE + " SET (itemId, status) = (?, ?) where id = ? RETURNING *";
    private Map<String, String> updateStatementMap;


    static final String DELETE_GROCERY_LIST = "DELETE FROM " + GROCERY_LISTS_TABLE + " WHERE id = ?;";
    static final String DELETE_GROCERY_ITEM_STATUS = "DELETE FROM " + GROCERY_ITEM_STATUS_TABLE + " WHERE id = ?";
    private Map<String, String> deleteStatementMap;

    static final String GET_ALL_LIST = "SELECT * FROM " + GROCERY_LISTS_TABLE;
    static final String GET_ALL_ITEMS = "SELECT * FROM " + GROCERY_ITEMS_TABLE;


    private Map <String, String> getAllResultMap;

    private Connection dbConnection;

    Statements(Connection dbConnection) {
        this.dbConnection = dbConnection;

        this.createTableStatementMap = new HashMap<>();
        this.createTableStatementMap.put(GROCERY_ITEMS_TABLE, GROCERY_ITEMS_CREATE_TABLE_STATEMENT);
        this.createTableStatementMap.put(GROCERY_LISTS_TABLE, GROCERY_LISTS_CREATE_TABLE_STATEMENT);
        this.createTableStatementMap.put(ACCOUNTS_TABLE, ACCOUNTS_TABLE_CREATE_STATEMENT);
        this.createTableStatementMap.put(GROCERY_ITEM_STATUS_TABLE, GROCERY_ITEM_STATUS_CREATE_TABLE_STATEMENT);

        this.createIndexStatementMap = new HashMap<>();
        this.createIndexStatementMap.put(GROCERY_ITEMS_TABLE, GROCERY_ITEMS_CREATE_INDEX_STATEMENT);
        this.createIndexStatementMap.put(GROCERY_LISTS_TABLE, GROCERY_LISTS_INDEX_STATEMENT);
        this.createIndexStatementMap.put(ACCOUNTS_TABLE, ACCOUNTS_CREATE_INDEX_STATEMENT);

        this.insertStatementMap = new HashMap<>();
        this.insertStatementMap.put(GROCERY_ITEMS_TABLE, GROCERY_ITEMS_INSERT_STATEMENT);
        this.insertStatementMap.put(GROCERY_LISTS_TABLE, GROCERY_LISTS_INSERT_STATEMENT);
        this.insertStatementMap.put(GROCERY_ITEM_STATUS_TABLE, GROCERY_ITEM_STATUS_INSERT_STATEMENT);
        this.insertStatementMap.put(ACCOUNTS_TABLE, ACCOUNTS_INSERT_NEW_USER);

        this.selectByIdStatementMap = new HashMap<>();
        this.selectByIdStatementMap.put(GROCERY_ITEMS_TABLE, GROCERY_ITEMS_SELECT_BY_ID_STATEMENT);
        this.selectByIdStatementMap.put(GROCERY_LISTS_TABLE, GROCERY_LISTS_SELECT_BY_ID_STATEMENT);
        this.selectByIdStatementMap.put(GROCERY_ITEM_STATUS_TABLE, GROCERY_ITEM_STATUS_SELECT_BY_ID_STATEMENT);
        this.selectByIdStatementMap.put(ACCOUNTS_TABLE, ACCOUNTS_SELECT_BY_ID);

        this.selectByNameStatementMap = new HashMap<>();
        this.selectByNameStatementMap.put(GROCERY_ITEMS_TABLE, GROCERY_ITEMS_SELECT_BY_NAME_STATEMENT);
        this.selectByNameStatementMap.put(GROCERY_LISTS_TABLE, GROCERY_LISTS_SELECT_BY_AUTHOR_STATEMENT);
        this.selectByNameStatementMap.put(ACCOUNTS_TABLE, ACCOUNTS_SELECT_BY_USERNAME);

        this.updateStatementMap = new HashMap<>();
        this.updateStatementMap.put(GROCERY_LISTS_TABLE, GROCERY_LISTS_UPDATE_STATEMENT);
        this.updateStatementMap.put(GROCERY_ITEM_STATUS_TABLE, GROCERY_ITEM_STATUS_UPDATE_STATEMENT);

        this.deleteStatementMap = new HashMap<>();
        this.deleteStatementMap.put(GROCERY_LISTS_TABLE, DELETE_GROCERY_LIST);
        this.deleteStatementMap.put(GROCERY_ITEM_STATUS_TABLE, DELETE_GROCERY_ITEM_STATUS);

        this.getAllResultMap = new HashMap<>();
        this.getAllResultMap.put(GROCERY_LISTS_TABLE, GET_ALL_LIST);
        this.getAllResultMap.put(GROCERY_ITEMS_TABLE, GET_ALL_ITEMS);
    }

    PreparedStatement getCheckTableExistsStatement() throws SQLException {
        return this.dbConnection.prepareStatement(CHECK_TABLE_EXISTS_STATEMENT);
    }

    PreparedStatement getCreateTableStatement(String tableName) throws SQLException {
        String statement = this.createTableStatementMap.get(tableName);
        if (statement == null) {
            return null;
        }
        return this.dbConnection.prepareStatement(statement);
    }

    PreparedStatement getCreateIndexStatement(String tableName) throws SQLException {
        String statement = this.createIndexStatementMap.get(tableName);
        if (statement == null) {
            return null;
        }
        return this.dbConnection.prepareStatement(statement);
    }

    PreparedStatement getInsertStatement(String tableName) throws SQLException {
        String statement = this.insertStatementMap.get(tableName);
        if (statement == null) {
            return null;
        }
        return this.dbConnection.prepareStatement(statement);
    }

    PreparedStatement getInsertStatementGroceryListNoItemStatement() throws SQLException {
        return this.dbConnection.prepareStatement(GROCERY_LISTS_INSERT_NOITEMS_STATEMENT);
    }

    PreparedStatement getSelectByIdStatement(String tableName) throws SQLException {
        String statement = this.selectByIdStatementMap.get(tableName);
        if (statement == null) {
            return null;
        }
        return this.dbConnection.prepareStatement(statement);
    }

    PreparedStatement getSelectByNameStatement(String tableName) throws SQLException {
        String statement = this.selectByNameStatementMap.get(tableName);
        if (statement == null) {
            return null;
        }
        return this.dbConnection.prepareStatement(statement);
    }

    PreparedStatement getDeleteStatement(String tableName) throws SQLException {
        String statement = this.deleteStatementMap.get(tableName);
        if (statement == null) {
            return null;
        }
        return this.dbConnection.prepareStatement(statement);
    }

    PreparedStatement getSelectItemFromGroceryListStatement() throws SQLException{
        return this.dbConnection.prepareStatement(GROCERY_LISTS_SELECT_ITEM_STATUS_ID_FROM_STATEMENT);
    }

    PreparedStatement getSelectByNameAndAuthorStatement() throws SQLException {
        return this.dbConnection.prepareStatement(this.GROCERY_LISTS_SELECT_BY_NAME_AND_AUTHOR_STATEMENT);
    }

    PreparedStatement getUpdateStatement(String tableName) throws SQLException {
        String statement = this.updateStatementMap.get(tableName);
        if (statement == null) {
            return null;
        }
        return this.dbConnection.prepareStatement(statement);
    }

    PreparedStatement getAllResult(String tableName) throws SQLException {
        String statement = this.getAllResultMap.get(tableName);
        if (statement == null) {
            return null;
        }
        return this.dbConnection.prepareStatement(statement);
    }

}
