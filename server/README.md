# Project

## Running
#### Normal
`./gradlew jar`

`java -jar build/libs/project-1.0-SNAPSHOT.jar`

#### Using Docker Compose
See the README in the parent directory.


## Developing
#### Routes
Add routes to the matching Java class in the `src/main/java/ca/sfu/tekkie/api/routes` package. For example, if the routes are for grocery list manipulation, add them to ListApi.java.

If there is no good class for the routes, make a new one.

All route methods should return JSON. Use gson for that.

Once the methods are created in a class in the `routes` package, hook them up in `ApiController.java` so they are called when the appropriate URL is visited.

#### DB
For creating database methods, place them in the DatabaseManager. Make sure there is a Java object in the `model` package for the object being added/pulled from the database.

All objects in the `model` must have a constructor with no arguments for gson to work properly. You can add other constructors for convenience too.