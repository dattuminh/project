'use strict';

module.exports = function(environment) {
  let ENV = {
    modulePrefix: 'grocery-navigator',
    environment,
    rootURL: '/',
    locationType: 'auto',
    // contentSecurityPolicyHeader: 'Content-Security-Policy',
    // contentSecurityPolicy: {
    //   // ... other stuff here
    //   'default-src': "'self' http://localhost:4567",
    // },
    EmberENV: {
      FEATURES: {
        // Here you can enable experimental features on an ember canary build
        // e.g. 'with-controller': true
      },
      EXTEND_PROTOTYPES: {
        // Prevent Ember Data from overriding Date.parse.
        Date: false
      }
    },

    APP: {
      apiURL: 'http://localhost:4567'
      // Here you can pass flags/options to your application instance
      // when it is created
    },
    'mapbox-gl': {
      accessToken: 'pk.eyJ1IjoiZG9vbTk0NSIsImEiOiJjamtkMmt0ZDYzaWlzM3JxcTFlcnEwdGx1In0.X1cjVVbUTwj0rs_C3P4Pfw',
      map: {
        style: 'mapbox://styles/mapbox/light-v9',
        zoom: 17.95,
        center: [-122.91002601385115, 49.27764673061019]
      },
      marker: {
        offset: [-1, -1]
      },
      popup: {
        offset: [ 0, -10 ],
        closeButton:false
      }
    }
  };

  if (environment === 'development') {
    // ENV.APP.LOG_RESOLVER = true;
    // ENV.APP.LOG_ACTIVE_GENERATION = true;
    // ENV.APP.LOG_TRANSITIONS = true;
    // ENV.APP.LOG_TRANSITIONS_INTERNAL = true;
    // ENV.APP.LOG_VIEW_LOOKUPS = true;
  }

  if (environment === 'test') {
    // Testem prefers this...
    ENV.locationType = 'none';

    // keep test console output quieter
    ENV.APP.LOG_ACTIVE_GENERATION = false;
    ENV.APP.LOG_VIEW_LOOKUPS = false;

    ENV.APP.rootElement = '#ember-testing';
    ENV.APP.autoboot = false;
  }

  if (environment === 'production') {
    // here you can enable a production-specific feature
  }

  return ENV;
};
