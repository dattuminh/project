import Base from 'ember-simple-auth/authenticators/base'
import $ from 'jquery'
import { Promise } from 'rsvp'
import { isEmpty } from '@ember/utils'
import { run } from '@ember/runloop'
import ENV from '../config/environment'

// Based on https://github.com/diegopoza/ember-jwt/blob/master/ember2app/app/authenticators/custom.js
export default Base.extend({
  endpoint: ENV.APP.apiURL,
  restore(data) {
    return new Promise(function(resolve, reject) {
      if (!isEmpty(data.token)) {
          resolve(data)
      } else {
          reject()
      }
    })
  },
  authenticate(options) {
    return new Promise((resolve, reject) => {
      $.ajax({
        url: this.endpoint + "/login",
        type: "POST",
        data: options,
        contentType: 'application/x-www-form-urlencoded'
      }).then(function(response) {
        response = JSON.parse(response)
        run(function() {
          resolve({
            token: response.token
          })
        })
      }, function(xhr) {
        let response = JSON.parse(xhr.responseText)
        run(function() {
          reject(response.error)
        })
      })
    })
  }
});