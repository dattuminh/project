import DS from 'ember-data';
import Ember from 'ember';

export default DS.Transform.extend({
  serialize: function(data) {
    var itemList = [], item;
    for (var i = 0; i < data.length; i++) {
        item = data[i];
        itemList.push([item.get('groceryItemId'), item.get('status')]);
    }
    return itemList;
    },
    deserialize: function(itemList) {
        var data = Ember.A(), item;
        for (var i = 0; i < itemList.length; i++) {
            item = itemList[i];
            data.push({ groceryItemId: item[0], status: item[1] });
        }
        return data;
    }
});

