import EmberError from '@ember/error';

// Taken from https://stackoverflow.com/questions/32456187/how-to-create-custom-error-classes-in-ember#32456643
let UnauthorizedError = function (errors, message) {
  EmberError.call(this, message);

  this.errors = errors || [
    {
      title: 'Unauthorized, please log in.',
      detail: message
    }
  ];
}

UnauthorizedError.prototype = Object.create(EmberError.prototype);

export default UnauthorizedError;