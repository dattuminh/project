import Controller from '@ember/controller';
import Ember from "ember";

export default Controller.extend({

  actions: {
    setMapBounds(map){
      map.setMaxBounds(map.getBounds());
    },
    checkItem(groceryItem){
      let itemStatus = this.get('itemStatusList').findBy('itemId', Number(groceryItem.id))
      let newStatus = !itemStatus.status
      itemStatus.set('status', newStatus)
      itemStatus.save()
    },
  }
});
