import Controller from '@ember/controller';
import groceryItemStatus from '../models/grocery-item-status';

export default Controller.extend({
  transitionToEdit: function(editRoute){
      this.transitionToRoute(editRoute);
  },

  actions: {
    createGroceryList(){
      let newName = this.get('newListName')
      let newDateCreated = ''
      let newDateModified = ''
      let newAuthor = 'frontend'
      let newRecord = this.store.createRecord('grocery-list', {
        name: newName,
        dateCreated: newDateCreated,
        dateModified: newDateModified,
        author: newAuthor,
      })

      var self = this;
      newRecord.save().then(function() {
        let route = '/edit/'
        let id = newRecord.get('id')
        self.transitionToEdit(route + id);
      })
    }
  }
});
