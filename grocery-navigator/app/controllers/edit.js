import Controller from '@ember/controller';
import { debounce, later } from '@ember/runloop';
import { computed } from '@ember/object';

export default Controller.extend({


    reload: function(){
        window.setTimeout(function(){
            window.location.reload(true);
        }, 200);
    },

    actions: {
        search() {
            debounce(this, this.actions.searchItem, 1000)
        },

        searchItem() {
            let searchText = this.get('searchText')
            this.set("dropdownActive", true)
            this.store.findAll("grocery-item").then((groceryItems) => {
                this.set("searchResults", groceryItems.filter((item) => {
                    if (item.name.includes(searchText) || item.description.includes(searchText)) {
                        return true;
                    }
                }))
            });
        },

        closeDropdown() {
            later(this, function() {
                this.set("dropdownActive", false)
            }, 200)
        },

        addItem(groceryItem){
            let list = this.get('list')
            let newItem = this.get('store').createRecord('grocery-item-status', {
                itemId: groceryItem.id,
                status: false
            })
            newItem.save().then((groceryItemStatus) => {
                list.get('itemList').addObject(Number(groceryItemStatus.id))
                list.save().then((list) => {
                    this.send('updateGroceryList')
                })
            })
            // this.reload()
        },

        getItemStatus(itemId){
            let itemStatus = this.get('itemStatusList').findBy('itemId', Number(itemId))
            return itemStatus.status
        },

        checkItem(groceryItem){
            let itemStatus = this.get('itemStatusList').findBy('itemId', Number(groceryItem.id))
            let newStatus = !itemStatus.status
            itemStatus.set('status', newStatus)
            itemStatus.save().then(() => this.send('updateGroceryList'))
        },

        deleteItem(groceryItem){
            let list = this.get('list')
            let itemStatus = this.get('itemStatusList').findBy('itemId', Number(groceryItem.id))
            list.get('itemList').removeObject(Number(itemStatus.id))
            list.save().then(() => {
                itemStatus.deleteRecord();
                itemStatus.get('isDeleted');
                itemStatus.save().then(() => this.send('updateGroceryList'))
            });
        },

        updateGroceryList () {
            this.store.query('grocery-item-status', {
                listId: this.get('list').get('id')
            }).then((itemStatusList) => {
                this.set('itemStatusList', itemStatusList);
            });
            this.store.query('grocery-item', {
                listId: this.get('list').get('id')
            }).then((itemList) => {
                this.set('itemList', itemList);
            })
        }
    }
});
