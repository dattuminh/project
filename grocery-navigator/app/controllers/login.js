import Controller from '@ember/controller'
import { inject as service } from '@ember/service'
import { computed } from '@ember/object'

export default Controller.extend({
  session: service('session'),
  username: null,
  password: null,
  hasError: false,
  usernameInputClass: computed('username', 'hasError', function() {
    let classes = ['input']
    let username = this.get('username')
    if ((username !== null && username.length === 0) || this.get('hasError')) {
      classes.push('is-danger')
    }
    return classes.join(' ')
  }),
  passwordInputClass: computed('password', 'hasError', function() {
    let classes = ['input']
    let password = this.get('password')
    if ((password !== null && password.length === 0) || this.get('hasError')) {
      classes.push('is-danger')
    }
    return classes.join(' ')
  }),

  actions: {
    authenticate() {
      let username = this.get('username')
      let password = this.get('password')

      if (username === null) {
        username = ''
        this.set('username', username)
      }

      if (password === null) {
        password = ''
        this.set('password', password)
      }

      if (username.length === 0 || password.length === 0) {
        this.set('errorMessage', 'Username and password cannot be blank.')
        return
      }

      this.get('session').authenticate('authenticator:jwt', {username: username, password: password})
        .then(() => {
          this.transitionToRoute('index')
        })
        .catch((error) => {
          this.set('hasError', 'true')
          this.set('errorMessage', error)
        })
    }
  }
});
