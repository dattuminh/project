import Controller from '@ember/controller'
import { inject as service } from '@ember/service'
import { computed } from '@ember/object'
import $ from 'jquery'
import { run } from '@ember/runloop'
import ENV from '../config/environment'

export default Controller.extend({
  session: service('session'),
  username: '',
  password: '',
  confirmPassword: '',
  initialUsername: true,
  initialPassword: true,
  hasError: false,
  usernameInputClass: computed('username', 'hasError', function() {
    let classes = ['input']
    if (this.get('initialUsername')) {
      this.set('initialUsername', false)
      return classes.join(' ')
    }
    let username = this.get('username')
    if (username.length === 0 || this.get('hasError')) {
      classes.push('is-danger')
    }
    return classes.join(' ')
  }),
  passwordInputClass: computed('password', 'confirmPassword', 'hasError', function() {
    let classes = ['input']
    if (this.get('initialPassword')) {
      this.set('initialPassword', false)
      return classes.join(' ')
    }
    let password = this.get('password')
    let confirmPassword = this.get('confirmPassword')

    let passwordValid = password.length !== 0
    let confirmPasswordValid = confirmPassword.length !== 0
    let passwordsMatch = password === confirmPassword
    if (!(passwordValid && confirmPasswordValid && passwordsMatch) || this.get('hasError')) {
      classes.push('is-danger')
    }
    if (!passwordsMatch) {
      this.set('passwordErrorMessage', 'Password and confirmation password must match')
    }
    else {
      this.set('passwordErrorMessage', '')
    }
    return classes.join(' ')
  }),

  actions: {
    register() {
      let username = this.get('username')
      let password = this.get('password')
      let confirmPassword = this.get('confirmPassword')

      if (username === null) {
        username = ''
        this.set('username', username)
      }

      if (password === null) {
        password = ''
        this.set('password', password)
      }

      if (confirmPassword === null) {
        confirmPassword = ''
        this.set('confirmPassword', confirmPassword)
      }

      if (username.length === 0 || password.length === 0 || confirmPassword.length === 0) {
        this.set('errorMessage', 'Username, password, and password confirmation cannot be blank.')
        return
      }

      if (password !== confirmPassword) {
        this.set('errorMessage', 'Password and password confirmation must match.')
        return
      }

      run(() => {
        $.ajax({
          url: ENV.APP.apiURL + "/register",
          type: "POST",
          data: {username: username, password: password},
          contentType: 'application/x-www-form-urlencoded'
        }).then(() => {
          run(() => {
            this.transitionToRoute('login')
          })
        }, (xhr) => {
          let response = JSON.parse(xhr.responseText)
          run(() => {
            this.set('hasError', 'true')
            this.set('errorMessage', response.error)
          })
        })
      })
    }
  }
});
