import DS from 'ember-data';
import { isPresent } from '@ember/utils';
import { computed } from '@ember/object';
import { inject as service } from '@ember/service';
import UnauthorizedError from '../errors/unauthorized-error'

export default DS.RESTAdapter.extend( {
  host: 'http://localhost:4567',
  namespace: 'api',
  session: service('session'),
  headers: computed('session.data.authenticated', function() {
    return {
      "Authorization": this.get('session.data.authenticated.token'),
    }
  }),
  handleResponse(status, headers, payload, requestData) {
    if (status === 401) {
      return new UnauthorizedError()
    }
    return payload
  }
});
