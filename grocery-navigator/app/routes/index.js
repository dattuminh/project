import Route from '@ember/routing/route';
import AuthenticatedRouteMixin from 'ember-simple-auth/mixins/authenticated-route-mixin';

export default Route.extend(AuthenticatedRouteMixin, {
  model(){
    return [
      {
        id: "1",
        name: "Leo"
      },
      {
        id: "2",
        name: "David"
      }
    ];
  }
});
