import Route from '@ember/routing/route';
import UnauthorizedError from '../errors/unauthorized-error'
import { inject as service } from '@ember/service';


export default Route.extend({
  session: service('session'),
  actions: {
    error: function(reason) {
      if (reason instanceof UnauthorizedError) {
        this.get('session').invalidate()
        this.transitionTo('login')
      }
    }
  }
});