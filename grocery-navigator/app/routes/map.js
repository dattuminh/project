import Route from '@ember/routing/route';

export default Route.extend({
    // TODO remove hardcoded items, use proper call
    // groceryList: this.get("store").findRecord('grocery-list', params.grocery_list_id);
    model(params){
        return this.get("store").findRecord('grocery-list', params.grocery_list_id);
    },

    setupController(controller, model){
        controller.set('list', model);
        this.store.query('grocery-item-status', {
            listId: controller.get('list').get('id')
        }).then(function(itemStatusList) {
            controller.set('itemStatusList', itemStatusList);
        });
        this.store.query('grocery-item', {
            listId: controller.get('list').get('id')
        }).then(function(itemList){
            controller.set('itemList', itemList);
        });
    }
});
