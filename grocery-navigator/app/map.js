mapboxgl.accessToken = 'pk.eyJ1IjoiZG9vbTk0NSIsImEiOiJjamtkMmt0ZDYzaWlzM3JxcTFlcnEwdGx1In0.X1cjVVbUTwj0rs_C3P4Pfw';
// This adds the map to your page
var map = new mapboxgl.Map({
  // container id specified in the HTML
  container: 'map',
  // style URL
  style: 'mapbox://styles/mapbox/light-v9',
  // initial position in [lon, lat] format
  center: [-77.034084, 38.909671],
  // initial zoom
  zoom: 14
});
