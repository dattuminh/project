import DS from 'ember-data';

export default DS.Model.extend({
  itemId: DS.attr('number'),
  status: DS.attr('boolean')
});
