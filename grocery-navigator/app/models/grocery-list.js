import DS from 'ember-data';

export default DS.Model.extend({
    name: DS.attr(),
    dateCreated: DS.attr('Date'),
    dateModified: DS.attr('Date'),
    author: DS.attr('String'),
    itemList: DS.attr('array')
});
