import DS from 'ember-data';

export default DS.Model.extend({
  // id: DS.attr('number'), (Removed due to ember implicitly declared it)
  name: DS.attr('string'),
  description: DS.attr('string'),
  latitude: DS.attr(),
  longtitude: DS.attr(),
  image: DS.attr()
});
