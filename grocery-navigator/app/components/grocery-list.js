import Component from '@ember/component';

export default Component.extend({
  actions: {
    checkItem(groceryItem){
        this.sendAction('itemChecked', groceryItem)
    },

    deleteItem(groceryItem){
        this.sendAction('itemDeleted', groceryItem)
    },
  }

});
