import EmberRouter from '@ember/routing/router';
import config from './config/environment';

const Router = EmberRouter.extend({
  location: config.locationType,
  rootURL: config.rootURL
});

//snake case = grocery_list_id

Router.map(function() {
  this.route('edit');
  this.route('edit', {path: '/edit/:grocery_list_id'});
  this.route('create');
  this.route('login');
  this.route('register');
  this.route('map', {path: '/map/:grocery_list_id'});
});

export default Router;
