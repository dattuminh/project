import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('transform:grocery-item-status', 'Unit | Transform | grocery item status', function(hooks) {
  setupTest(hooks);

  // Replace this with your real tests.
  test('it exists', function(assert) {
    let transform = this.owner.lookup('transform:grocery-item-status');
    assert.ok(transform);
  });
});
